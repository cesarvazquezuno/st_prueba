import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:crypto/crypto.dart' as crypto;
import 'package:shared_preferences/shared_preferences.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Lista de Personajes de Marvel',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      initialRoute: '/',
      routes: {
        '/': (context) => HomePage(),
        '/detalle': (context) => DetallePage(),
      },
    );
  }
}

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  late List<dynamic> characters;
  late List<dynamic> filteredCharacters;
  int offset = 0;
  int limit = 20;
  bool isLoading = false;
  final ScrollController _scrollController = ScrollController();
  int totalCharacters = 0;
  TextEditingController searchController = TextEditingController();

  @override
  void initState() {
    super.initState();
    characters = [];
    filteredCharacters = [];
    _loadCharactersFromStorage();

    _scrollController.addListener(() {
      if (_scrollController.position.pixels == _scrollController.position.maxScrollExtent) {
        _loadCharacters();
      }
    });

    searchController.addListener(() {
      _filterCharacters();
    });
  }

  Future<void> _loadCharactersFromStorage() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final String? charactersJson = prefs.getString('characters');

    if (charactersJson != null) {
      setState(() {
        characters = json.decode(charactersJson);
        filteredCharacters = List.from(characters);
      });
    }

    _loadCharacters();
  }

  Future<void> _loadCharacters() async {
    setState(() {
      isLoading = true;
    });

    final moreCharacters = await fetchMarvelCharacters(offset, limit);
    totalCharacters = moreCharacters['total'];
    final List<dynamic> newCharacters = moreCharacters['results'];

    setState(() {
      characters.addAll(newCharacters);
      filteredCharacters.addAll(newCharacters);
      offset += limit;
      isLoading = false;
    });

    _saveCharactersToStorage();
  }

  void _filterCharacters() {
    setState(() {
      filteredCharacters = characters.where((character) {
        final name = character['name'].toString().toLowerCase();
        final query = searchController.text.toLowerCase();
        return name.contains(query);
      }).toList();
    });
  }

  Future<void> _saveCharactersToStorage() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('characters', json.encode(characters));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(
        totalCharacters: totalCharacters,
        loadedCharacters: characters.length,
        searchController: searchController,
      ),
      body: ListView.builder(
        controller: _scrollController,
        itemCount: filteredCharacters.length + 1,
        itemBuilder: (context, index) {
          if (index < filteredCharacters.length) {
            final character = filteredCharacters[index];
            return ListTile(
              leading: CircleAvatar(
                backgroundImage: NetworkImage(character['thumbnail']['path'] +
                    '.' +
                    character['thumbnail']['extension']),
              ),
              title: Text(character['name']),
              subtitle: Text(character['description'] != ''
                  ? character['description']
                  : 'Descripción no disponible'),
              onTap: () {
                Navigator.pushNamed(
                  context,
                  '/detalle',
                  arguments: character,
                );
              },
            );
          } else {
            if (isLoading) {
              return Center(
                child: CircularProgressIndicator(),
              );
            } else {
              // Si ya no hay más personajes, muestra un mensaje
              return Center(
                child: filteredCharacters.isEmpty
                    ? Text('No se han encontrado personajes.')
                    : Text('Ya has cargado todos los personajes.'),
              );
            }
          }
        },
      ),
    );
  }
}

class CustomAppBar extends StatelessWidget implements PreferredSizeWidget {
  final int totalCharacters;
  final int loadedCharacters;
  final TextEditingController searchController;

  CustomAppBar({
    required this.totalCharacters,
    required this.loadedCharacters,
    required this.searchController,
  });

  @override
  Size get preferredSize => Size.fromHeight(kToolbarHeight + 56); // Altura del AppBar más altura del TextField

  @override
  Widget build(BuildContext context) {
    return AppBar(
      title: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text('Marvel Characters'),
          Text(
            'Total: $totalCharacters - Cargados: $loadedCharacters',
            style: TextStyle(fontSize: 12),
          ),
        ],
      ),
      bottom: PreferredSize(
        preferredSize: Size.fromHeight(56),
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: TextField(
            controller: searchController,
            decoration: InputDecoration(
              hintText: 'Buscar por nombre...',
              border: OutlineInputBorder(),
            ),
          ),
        ),
      ),
    );
  }
}

class DetallePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final character = ModalRoute.of(context)!.settings.arguments as Map<String, dynamic>;

    return Scaffold(
      appBar: AppBar(
        title: Text(character['name']),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            CircleAvatar(
              backgroundImage: NetworkImage(character['thumbnail']['path'] +
                  '.' +
                  character['thumbnail']['extension']),
              radius: 50,
            ),
            SizedBox(height: 20),
            Text(
              character['description'] != '' ? character['description'] : 'Descripción no disponible',
              textAlign: TextAlign.center,
            ),
          ],
        ),
      ),
    );
  }
}

Future<Map<String, dynamic>> fetchMarvelCharacters(int offset, int limit) async {
  final publicKey = '530549c06a2651508eba035b4a6a3cce';
  final privateKey = 'ef118fffeff37954df0f7cffd535245ae1ef7e3c';
  final timeStamp = DateTime.now().millisecondsSinceEpoch.toString();
  final hash = generateMd5('$timeStamp$privateKey$publicKey');
  final url =
      'https://gateway.marvel.com/v1/public/characters?ts=$timeStamp&apikey=$publicKey&hash=$hash&offset=$offset&limit=$limit';

  final response = await http.get(Uri.parse(url));

  if (response.statusCode == 200) {
    final Map<String, dynamic> data = json.decode(response.body);
    final total = data['data']['total'];
    final results = data['data']['results'];
    return {'total': total, 'results': results};
  } else {
    throw Exception('Failed to load Marvel characters');
  }
}

String generateMd5(String input) {
  return crypto.md5.convert(utf8.encode(input)).toString();
}
